//Run on load
let array1 = [1,2,3,4,5];
document.getElementById("array").innerHTML = array1;
//create all dropdown
function create(){
    createDD("queueNumber","dropdown1");
    createDD("queueNumber2","dropdown2");
    createDD("rangeNumber","dropdown3");
    }
//Recreate all dropdown
function clearAndCreate(){
    document.getElementById("queueNumber").remove();
    document.getElementById("queueNumber2").remove();
    document.getElementById("rangeNumber").remove();
    create();
    }
//Create Dropdown(s)
function createDD(x,y){    
    var a = document.createElement("select"),att = document.createAttribute("id");
    att.value = x;
    a.setAttributeNode(att); 
    var nmb = document.getElementById(y);
    nmb.insertBefore(a,nmb.childNodes[0]);
    var z = array1.length; 
        if(x == "queueNumber"){
            z += 1;
        }
        if(x == "rangeNumber"){
            var q = parseInt(document.getElementById("queueNumber2").value);
            z = z - q + 1;
        }
        for (var i=0; i<z; i++){
            var b = document.createElement("option");
            a.appendChild(b); 
            var att2 = document.createAttribute("value");
            att2.value = i+1;
            b.setAttributeNode(att2); 
            b.innerHTML = i+1;
        }
    }
//Validate Data
function validate(input) {
    var x = input.value;
    //No decimals & space
        if (input.value.match(/[\s.]/g)){
        input.value=input.value.replace(/[\s.]/g,'');
        } else if (x.length >= 0){
            //max length = 6
            input.value = x.substr(0, 6);
        }
    }
//Insert Data
function insertData(){
    var queueNumber = document.getElementById("queueNumber");
    var z = document.getElementById("input").value;
    if (z =="") {alert("Can't be empty!");return false;}
        //add data to the array
        var x = array1.unshift(z);
        //place the data based on queueNumber
            for (i=0; i<queueNumber.value; i++){
            array1[i]=array1[i+1];
            }
            array1[queueNumber.value-1]= z;
        document.getElementById("array").innerHTML = array1;
            //Delete input data
            document.getElementById("input").value = "";
            clearAndCreate();
    }
//Remove Data by Range
function removeData(){
    //remove Data from array
    array1.splice(queueNumber2.value-1,rangeNumber.value);    
    //place the data based on queueNumber        
    document.getElementById("array").innerHTML = array1;
    clearAndCreate();             
    }
//Auto Update D3 base on D2
function autoUpdateD3(){
    document.getElementById("rangeNumber").remove();
    createDD("rangeNumber","dropdown3");
    }