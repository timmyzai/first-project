class Solution_2 {
  static ListNode x, y;

  static class ListNode {
    int data;
    ListNode next;
    ListNode(int num) { data = num;}
  }
  public static void main(String[] args) {
    Solution_2 test = new Solution_2();

    test.x = new ListNode(7);
    test.x.next = new ListNode(1);
    test.x.next.next = new ListNode(6);
    String first_set = test.x.data + "->" + test.x.next.data + "->" + test.x.next.next.data;

    test.y = new ListNode(5);
    test.y.next = new ListNode(9);
    test.y.next.next = new ListNode(2);
    String second_set = test.y.data + "->" + test.y.next.data + "->" + test.y.next.next.data;

    System.out.println("first_set: (" + first_set + ")");
    System.out.println("second_set: (" + second_set + ")");

    int result = test.addTwoNumbers(x, y);
  }

  public int addTwoNumbers(ListNode l1, ListNode l2) {
    String l1_num =  String.valueOf(l1.next.next.data) + String.valueOf(l1.next.data) + String.valueOf(l1.data);
    String l2_num =  String.valueOf(l2.next.next.data) + String.valueOf(l2.next.data) + String.valueOf(l2.data);
    
    int num1 = Integer.parseInt(l1_num); 
    int num2 = Integer.parseInt(l2_num);
    int sum = num1 + num2;
    
    String result_set = convert_sum_to_linked_node(String.valueOf(sum));

    System.out.println("-----------");
    System.out.println(num1 + " + " + num2 + " = " + sum);
    System.out.println("result_set: (" + result_set + ")");

    return sum;
  }

  public static String convert_sum_to_linked_node(String str){
    char a = str.charAt(0);
    char b = str.charAt(1);
    char c = str.charAt(2);

    int first_num = a - '0';
    int second_num = b - '0';
    int third_num = c - '0';

    ListNode node = new ListNode(first_num);
    node.next = new ListNode(second_num);
    node.next.next = new ListNode(third_num);

    return node.data + "->" + node.next.data + "->" + node.next.next.data;
  }
}