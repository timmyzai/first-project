//Declare Necessary Variables
let array1 = [];
let nmbToShow = 4;
let showImg = false;
//mementos for Undo and Redo
const mementos = [];
const mementos2 = [];
//mementos for adjustment
const mementos3 = [];
//set up JSON data
var anime = 
{
    "demonSlayer":[
      {"firstName":"Tanjiro", "lastName":"Kamado"},
      {"firstName":"Nezuko", "lastName":"Kamado"},
      {"firstName":"Zenitsu", "lastName":"Agatsuma"},
      {"firstName":"Inosuke", "lastName":"Hashibira"},
      {"firstName":"Kanao", "lastName":"Tsuyuri"}
    ]
};
//Run on load
function onLoad(){
    //create DD first if not the remove() in generateData() will delete nothing
    createDD();
    generateData(`demonSlayer`);
    mementos.pop();
    document.getElementById("undoCount").innerHTML = mementos.length;
}
function autoSubmit(){
    var inputNmb = document.getElementById("inputNmb").value;
    nmbDataToShow(inputNmb);
}
//set number of data to show
function nmbDataToShow(nmb){
    if(nmb == "All"){nmbToShow = array1.length;} else nmbToShow = nmb;
    //recreate Array and reset adjustment
    removeArray();
    createArray();
    adjustment = 0;
    //Reapply Hint
    var previousAction = mementos3.pop();
    if(previousAction == `insertHint`){insertHint();}
    if(previousAction == `removeHint`){removeHint();}
    if(previousAction == `replaceHint`){replaceHint();}
    document.getElementById("inputNmb").value = nmbToShow;
    if(nmb == "All"){nmbToShow = "All";}
}
//create array data
function createArray(){
    var array = document.getElementById(`array`);
    var previousNext = document.getElementById(`previousNext`);
    var arrLength = array1.length;
    var all = nmbToShow;
    if(all == "All"){nmbToShow = arrLength;}
    for(var i = 0; i < nmbToShow; i++){
        if(i == arrLength){break;};
        //myCarousel div
        var div3 = document.createElement("div"),id = document.createAttribute("id"),cls = document.createAttribute("class"),dataInterval = document.createAttribute("data-interval");
        id.value = `myCarousel${i+1}`;
        cls.value = `myCarousel carousel slide`;
        dataInterval.value = `false`;
        div3.setAttributeNode(id);
        div3.setAttributeNode(cls);
        div3.setAttributeNode(dataInterval);
        div3.style.cssText = `width: 100px; height: 100px; border: 5px solid black; border-radius: 20%; margin: 10px; display: inline-block; font-weight: bold; line-height: 90px;`
        array.appendChild(div3);
        //Carousel Inner div
        var div2 = document.createElement("div"),id = document.createAttribute("id"),cls = document.createAttribute("class"),role = document.createAttribute("role");
        id.value = `carousel-inner${i+1}`;
        cls.value = `carousel-inner`;
        role.value = `listbox`;
        div2.setAttributeNode(id);
        div2.setAttributeNode(cls);
        div2.setAttributeNode(role);
        //Carousel Indicator Div
        var divIndicators = document.createElement("div"),id = document.createAttribute("id");
        id.value = `carousel-indicators${i+1}`;
        divIndicators.setAttributeNode(id);
        var ol = document.createElement("ol"),cls = document.createAttribute("class");
        cls.value = `carousel-indicators`;
        ol.setAttributeNode(cls);
        //Start create inner content, Divs append their child
        for (var j = 0; j < arrLength; j++){
            //create div Item
            var div1 = document.createElement("div"),cls = document.createAttribute("class");
            if(i==j && j == j){cls.value = `item active`;} else cls.value = `item`;
            div1.setAttributeNode(cls);
            //create div array data
            var divArrayData = document.createElement("div"),cls = document.createAttribute("class");
            cls.value = `data`;
            divArrayData.setAttributeNode(cls);
            //show data
            divArrayData.innerHTML = array1[j];
            //div1 Items append Data
            div1.appendChild(divArrayData);
            div2.appendChild(div1);
            div3.appendChild(div2);
            //if need to show image then only create
            if(showImg == true){
                //create div images
                var divImg = document.createElement("div"),cls = document.createAttribute("class");
                cls.value = `overlay`;
                divImg.setAttributeNode(cls);
                //create img
                var img = document.createElement("img"),src = document.createAttribute("src");
                img.style.cssText = `width: 100%; height: 101%; border-radius: 17%;`
                src.value = ``;
                var k = 0;
                if(array1[i] == anime.demonSlayer[k].firstName){
                    src.value = `img/${anime.demonSlayer[k].firstName}.jpg`;
                } 
                else {
                    while(array1[i] != anime.demonSlayer[k].firstName){
                        k++;
                        if(anime.demonSlayer[k] == undefined){
                            var cls = document.createAttribute("class");
                            cls.value = `disappear`;
                            divImg.setAttributeNode(cls);
                            break;
                        }
                        if(array1[i] == anime.demonSlayer[k].firstName){
                            src.value = `img/${anime.demonSlayer[k].firstName}.jpg`;
                            break;
                        }
                    }
                }
                img.setAttributeNode(src);
                //add hover effect to div2 - carousel-inner
                var css = document.createTextNode(`#carousel-inner${i+1}:hover .overlay{opacity: 1;}`);
                var head = document.getElementsByTagName('head'), style = document.createElement('style');
                style.appendChild(css);
                head[0].appendChild(style);
                //div1 Items append div Img
                divImg.appendChild(img);
                div1.appendChild(divImg);
                }
            //Carousel Indicator appends Indicators (li)
            var li = document.createElement("li"), dataTarget = document.createAttribute("data-target"), dataSlideTo = document.createAttribute("data-slide-to"), cls = document.createAttribute("class");
            dataTarget.value = `#myCarousel${i+1}`;
            dataSlideTo.value = `${j}`;
            if(i==j && j == j){cls.value = `active`;} else cls.value = ``;
            li.setAttributeNode(dataTarget);
            li.setAttributeNode(dataSlideTo);
            li.setAttributeNode(cls);
            ol.appendChild(li);
            divIndicators.appendChild(ol);
            div3.appendChild(divIndicators);
        }
    }
    //hide Next Previous and indicator when nmbToShow >= array length
    if(arrLength <= nmbToShow){
        previousNext.style.display = `none`;
        for(var i = 0; i < arrLength; i++){
        document.getElementById(`carousel-indicators${i+1}`).style.display = `none`;
        }
    } else previousNext.style.display = `block`;
    //remove extra duplicated style
    var styleLength = document.getElementsByTagName('style').length;
    if(styleLength > 0){
        for(var i = styleLength - 1; i >= 0; i--){
            if(i-1 >= 0){
                var lastChild = document.getElementsByTagName('style').item(i).innerText;
                var secondLastChild = document.getElementsByTagName('style').item(i-1).innerText;
                if(secondLastChild == lastChild){
                    document.getElementsByTagName('style').item(i).remove();
                }
            }
        }
    }
    if(all == "All"){nmbToShow = "All";}
}
function removeArray(){
    //clean style in HTML
    var styleLength = document.getElementsByTagName('style').length;
    if(styleLength != 0){
        for(var i = styleLength; i > 0; i--){
            document.getElementsByTagName('style').item(i-1).remove();
        }
    }
    var array = document.getElementById("array");
    while(array.lastChild.id !== `previousNext`){
        array.removeChild(array.lastChild);
    }
}
//create all dropdown
function createDD(){
    generateDD("queueNmb","dropdown1");
    generateDD("queueNmb2","dropdown2");
    generateDD("queueNmb2_2","dropdown2_2");
    generateDD("rngNmb","dropdown3");
}
//Recreate all dropdown
function clearAndCreate(){
    clearInterval(interval);
    removeArray();
    createArray();
    document.getElementById("queueNmb").remove();
    document.getElementById("queueNmb2").remove();
    document.getElementById("queueNmb2_2").remove();
    document.getElementById("rngNmb").remove();
    document.getElementById("input").value = "";
    document.getElementById("input2").value = "";
    document.getElementById("nmbOfInsert").value = "1";
    document.getElementById("single").checked = true;
    document.getElementById("box1").style.display = `none`;
    createDD();
    adjustment = 0;
    mementos3.length = 0;
}
//Create Dropdown(s)
function generateDD(nmb,dd){
    var arrLength = array1.length;
    var select = document.createElement("select"),att = document.createAttribute("id");
    att.value = nmb;
    select.setAttributeNode(att); 
    var dropdown = document.getElementById(dd);
    dropdown.insertBefore(select,dropdown.childNodes[0]);
        //generate queueNmb DD
        if(nmb == "queueNmb"){
            //define arrLength value for queueNmb DD
            arrLength += 1;
            //generate DD
            for (var i = 0; i < arrLength; i++){
                var option = document.createElement("option");
                select.appendChild(option); 
                var att2 = document.createAttribute("value");
                att2.value = i + 1;
                option.setAttributeNode(att2);
                //what to display
                option.innerHTML = i + 1;
            }
        }
        //generate rngNmb DD
        if(nmb == "rngNmb"){
            //define arrLength value for rngNmb DD
            var queueNmb2 = parseInt(document.getElementById("queueNmb2").value);
            var arrLength = arrLength - queueNmb2 + 1;
            //generate DD
            for (var i = 0; i < arrLength; i++){
                var option = document.createElement("option");
                var att2 = document.createAttribute("value");
                var id = document.createAttribute("id");
                att2.value = i + 1;
                id.value = i + queueNmb2;
                select.appendChild(option); 
                option.setAttributeNode(att2);
                option.setAttributeNode(id);
                //what to display
                option.innerHTML =array1[i + queueNmb2 - 1];
                }
            }
        if(nmb == "queueNmb2" || nmb == "queueNmb2_2"){
            for (var i = 0; i < arrLength; i++){
                var option = document.createElement("option");
                select.appendChild(option); 
                var att2 = document.createAttribute("value");
                att2.value = i + 1;
                option.setAttributeNode(att2);
                //what to display
            option.innerHTML =array1[i];
            }
        }
}
//Validate Data
function validate(input) {
    var x = input.value;
    //No decimals & space
        if (input.value.match(/[\s.]/g)){
            input.value=input.value.replace(/[\s.]/g,'');
        } else if (x.length >= 0){
            //max length = 8
            input.value = x.substr(0, 8);
        }
}
//Validate Number
function validateNmb(input) {
    var x = input.value;
    var regex = /^[0-9]+$/
    var isValid = regex.test(input.value);
    //NUmbers only
    if (!isValid){
        input.value = "";
    } else if (x.length >= 0){
        //max length = 2
        input.value = x.substr(0, 2);
    }
}
//Insert Data
function insertData(){
    var input = document.getElementById("input").value;
    if (input =="") {alert("Can't be empty!");return false;}
    var arrLength = array1.length;
    var queueNmb = parseInt(document.getElementById("queueNmb").value);
    var nmbOfInsert = parseInt(document.getElementById("nmbOfInsert").value);
    if(document.getElementById("single").checked){nmbOfInsert = 1;};
    var all = nmbToShow;
    if(all == "All"){nmbToShow = arrLength + nmbOfInsert;}
        //save mementos
        mementos.push([...array1]);
        document.getElementById("undoCount").innerHTML = mementos.length;
        //add data to the array
        for(var i = 0; i < nmbOfInsert;i++){
        array1.unshift(input);
        }
        //rearrange the array to put the data at correct location
        var moveData = array1.splice(nmbOfInsert,queueNmb-1);
        for(var i=moveData.length; i > 0 ;i--){
                array1.unshift(moveData.pop());
        }
    clearAndCreate();
    clearRedo();
    if(all == "All"){nmbToShow = "All";}
}
//Remove Data by Range
function removeData(){
    if (array1.length == 0){return false;}
    var queueNmb2 = document.getElementById("queueNmb2");
    var rngNmb = document.getElementById("rngNmb");
    //save mementos
    mementos.push([...array1]);
    document.getElementById("undoCount").innerHTML = mementos.length;
    //remove Data
    array1.splice(queueNmb2.value-1,rngNmb.value)
    if(nmbToShow == "All"){
        var all = array1.length;
        clearAndCreate(all);
        return false;
    }
    clearAndCreate();
    clearRedo();
}
//Replace Existing Data
function replaceData(){
    var input2 = document.getElementById("input2").value;
    if (input2 =="") {alert("Can't be empty!");return false;}
    //save mementos
    mementos.push([...array1]);
    document.getElementById("undoCount").innerHTML = mementos.length;
    //replace data
    var queueNmb2_2 = document.getElementById("queueNmb2_2");
    array1.fill(input2,queueNmb2_2.value-1,queueNmb2_2.value);
    //Delete input data
    document.getElementById("input2").value = "";
    clearAndCreate();
    clearRedo();
}
//Auto Update D3 base on D2
function autoUpdateD3(){
    var rngNmb = document.getElementById("rngNmb");
    //get selected data before auto update dropdown 3
    var prevSelected = rngNmb.options[rngNmb.selectedIndex].id;
    //update dropdown 3
    rngNmb.remove();
    generateDD("rngNmb","dropdown3");
    //after autoUpdateD3, select previous selected
    var rngNmb = document.getElementById("rngNmb");
    var currentSelected = rngNmb.options[rngNmb.selectedIndex].id;
        for(var i = 1; currentSelected != prevSelected;i++){
            //if current selected != previous selected, check next option
            rngNmb.value = `${i}`;
            var currentSelected = rngNmb.options[rngNmb.selectedIndex].id;
            //if current selected found, then stop
            if(currentSelected == prevSelected){return false;}
            //if not found, then select the first option
            if(i == rngNmb.length){
                rngNmb.value = `${1}`;
                return false;
            }
        }
}
//Generate Random Data
function generateData(obj){
    //save mementos
    mementos.push([...array1]);
    document.getElementById("undoCount").innerHTML = mementos.length;
    //reset and generate data
    array1 = [];
    let arrayOpt = [`numbers`,`alphabets`,`randomData`,`demonSlayer`];
    if(obj == null||undefined){obj = arrayOpt[Math.floor(Math.random() * arrayOpt.length)];}
    for(var i = 0; i < 5;i++){
        if(obj == `numbers`){array1 = [1,2,3,4,5]; showImg = false;}
        if(obj == `alphabets`){array1 = ["a","b","c","d","e"]; showImg = false;}
        if(obj == `randomData`|| obj == null){array1.push(generatePassword()); showImg = false;}
        if(obj == `demonSlayer`){array1.push(anime.demonSlayer[i].firstName); showImg = true;}
    }
    clearAndCreate();
}
function generatePassword(){
    var charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var charset2 = "0123456789"
    var length = 6;
    var password = "";
    for (var i = 0, n = charset.length; i < length-2; i++) {
        password += charset.charAt(Math.floor(Math.random() * n));
    }
    for (var i = 0, n = charset2.length; i < length-4; i++) {
        password += charset2.charAt(Math.floor(Math.random() * n));
    }
    return password;
}
//Undo Function
function undo(){
    if(mementos.length < 1){
        alert(`No undo move.`)
        return false;
    }
    //save mementos2
    mementos2.push([...array1]);
    //undo
    var undoMementos = [];
    undoMementos = mementos.pop();
    // console.log(undoMementos);
    array1 = [...undoMementos];
    document.getElementById("undoCount").innerHTML = mementos.length;
    document.getElementById("redoCount").innerHTML = mementos2.length;
    clearAndCreate();
}
//Redo Function
function redo(){
    // alert(`feature coming soon`);
    if(mementos2.length < 1){
        alert(`No redo move.`)
        return false;
    }
    //save mementos
    mementos.push([...array1]);
    //redo
    var redoMementos = [];
    redoMementos = mementos2.pop();
    array1 = [...redoMementos];
    document.getElementById("undoCount").innerHTML = mementos.length;
    document.getElementById("redoCount").innerHTML = mementos2.length;
    clearAndCreate();
}
//Clear Redo after other action beside Undo is triggered
function clearRedo(){
    mementos2.length = 0;
    document.getElementById("redoCount").innerHTML = mementos2.length;
}
//Reset All
function clearData(){
    if(array1.length < 1){
        clearAndCreate();
        return false;
    }
    //save mementos
    mementos.push([...array1]);
    document.getElementById("undoCount").innerHTML = mementos.length;
    //clear array
    array1 = [];
    clearAndCreate();
    clearRedo();
}
//display hints by border color
let interval = null;
let adjustment = 0;
let running = false;
function adjust(x){
    // console.log(`click`);
    if(running == true){return false;}
    running = true;
    //disable the Next and Previous button
    setTimeout(() => {document.getElementById(`previousNext`).style.pointerEvents = "none";},100)
    //set range for the adjustment
    adjustment = adjustment + x;
    if(Math.abs(adjustment) == array1.length){adjustment = 0;}
    //adjust hints and image after the slider animation
    setTimeout(adjustHints,750)
}
//adjust hints location
function adjustHints(){
    //check and execute previous triggered Hint
    var previousAction = mementos3.pop();
    if(previousAction == `insertHint`){insertHint();}
    if(previousAction == `removeHint`){removeHint();}
    if(previousAction == `replaceHint`){replaceHint();}
    console.log("adjust data");
    if(showImg == false){
    document.getElementById(`previousNext`).style.pointerEvents = "auto";
    running = false;
    return false;
    }
    //adjust image
    adjustImage();
}
//adjust image
function adjustImage(){
    for(var i = 0; i < nmbToShow; i++){
        //get current data display on the screen
        var inner = document.getElementById(`carousel-inner${i + 1}`);
        var activeItem = inner.querySelector(`.active`);
        var data = activeItem.querySelector(`.data`).innerText;
            //set image based on the data
            var k = 0;
            if(data == anime.demonSlayer[k].firstName){
                console.log(`Slide${i+1} data is ${data}`);
                for(var j = 0; j < array1.length; j++){
                var img = inner.getElementsByTagName('img')[j];
                img.src = `img/${data}.jpg`;
                    if(inner.querySelector(`.disappear`) != null){
                        var cls = document.createAttribute("class");
                        cls.value = `overlay`;
                        inner.querySelector(`.disappear`).setAttributeNode(cls);
                    }
                }
            }
            else{
                while(data != anime.demonSlayer[k].firstName){
                    k++;
                    if(anime.demonSlayer[k] == undefined){
                        console.log(`Slide${i+1} data is ${data}`);
                        for(var j = 0; j < array1.length; j++){
                            var img = inner.getElementsByTagName('img')[j];
                            img.src = ``;
                                if(inner.querySelector(`.overlay`) != null){
                                    var cls = document.createAttribute("class");
                                    cls.value = `disappear`;
                                    inner.querySelector(`.overlay`).setAttributeNode(cls);
                                }
                            }
                        break;
                    }
                    if(data == anime.demonSlayer[k].firstName){
                        console.log(`Slide${i+1} data is ${data}`);
                        for(var j = 0; j < array1.length; j++){
                            var img = inner.getElementsByTagName('img')[j];
                            img.src = `img/${data}.jpg`;
                                if(inner.querySelector(`.disappear`) != null){
                                    var cls = document.createAttribute("class");
                                    cls.value = `overlay`;
                                    inner.querySelector(`.disappear`).setAttributeNode(cls);
                                }
                            }
                        break;
                    }
                }
            }
        }
    console.log("adjust image");
    document.getElementById(`previousNext`).style.pointerEvents = "auto";
    running = false;
}
//Hints
function insertHint(){
    clearInterval(interval);
    if(array1.length == 0){return false;}
    //remain show all, if "All" is selected
    var all = nmbToShow;
    if(all == "All"){nmbToShow = array1.length;}
    //save mementos3 and reset hint
    mementos3.push(`insertHint`);
    resetBorder();
    //adjustment
    var queueNmb = parseInt(document.getElementById("queueNmb").value);
    var afterAdjust = queueNmb + adjustment;
    if(adjustment < 0 && afterAdjust <= 0){
            afterAdjust = Math.abs(afterAdjust + array1.length);
    }
    if(adjustment > 0 && afterAdjust > array1.length){
            afterAdjust = Math.abs(afterAdjust - array1.length);
    }
    //find insert location
    if(afterAdjust == 1){//Most Left
        document.getElementById(`myCarousel1`).style.borderLeftColor = `blue`;
        if(all == "All"){nmbToShow = "All";}
        return false;
    }
    if(afterAdjust == nmbToShow + 1){//Most Right
        document.getElementById(`myCarousel${nmbToShow}`).style.borderRightColor = `blue`;
        if(all == "All"){nmbToShow = "All";}
        return false;
    }
    if(afterAdjust > nmbToShow){//Out of range
        resetBorder();
        if(all == "All"){nmbToShow = "All";}
        return false;
    }
    //In between 2 data
    document.getElementById(`myCarousel${afterAdjust-1}`).style.borderRightColor = `blue`;
    document.getElementById(`myCarousel${afterAdjust}`).style.borderLeftColor = `blue`;
    if(all == "All"){nmbToShow = "All";}
}
function removeHint(){
    clearInterval(interval);
    if(array1.length == 0){return false;}
    //remain show all, if "All" is selected
    var all = nmbToShow;
    if(all == "All"){nmbToShow = array1.length;}
    //save mementos3 and reset hint
    mementos3.push(`removeHint`);
    resetBorder();
    //adjustment
    var queueNmb2 = parseInt(document.getElementById("queueNmb2").value);
    var afterAdjust = queueNmb2 + adjustment;
    if(adjustment < 0 && afterAdjust <= 0){
            afterAdjust = Math.abs(afterAdjust + array1.length);
    }
    if(adjustment > 0 && afterAdjust > array1.length){
            afterAdjust = Math.abs(afterAdjust - array1.length);
    }
    //find remove locations
    var rngNmb = parseInt(document.getElementById("rngNmb").value);
    var normalRng = afterAdjust + rngNmb - 1;
    if(normalRng > array1.length){//from 1st slide (hint on both side)
        for(var j = 0; j < normalRng - array1.length; j++){
            document.getElementById(`myCarousel${j+1}`).style.border = `5px solid blue`;
        }
    }
    //from targeted to end
    for(var i = afterAdjust, j = rngNmb; i < i + j; i++, j--){
        if(i > nmbToShow){//out of range
            if(all == "All"){nmbToShow = "All";}
            interval = setInterval(() => {blink()}, 650);
            return false;
        }
        document.getElementById(`myCarousel${i}`).style.border = `5px solid blue`;
    }
    //blink
    interval = setInterval(() => {blink()}, 650);
    if(all == "All"){nmbToShow = "All";}
}
function replaceHint(){
    clearInterval(interval);
    if(array1.length == 0){return false;}
    //remain show all, if "All" is selected
    var all = nmbToShow;
    if(all == "All"){nmbToShow = array1.length;}
    //save mementos3 and reset hint
    mementos3.push(`replaceHint`);
    resetBorder();
    //adjustment
    var queueNmb2_2 = parseInt(document.getElementById("queueNmb2_2").value);
    var afterAdjust = queueNmb2_2 + adjustment;
    if(adjustment < 0 && afterAdjust <= 0){
            afterAdjust = Math.abs(afterAdjust + array1.length);
    }
    if(adjustment > 0 && afterAdjust > array1.length){
            afterAdjust = Math.abs(afterAdjust - array1.length);
    }
    //change border color for selected data
    if(afterAdjust > nmbToShow){//Out of range
        resetBorder();
        if(all == "All"){nmbToShow = "All";}
        return false;
    }
    document.getElementById(`myCarousel${afterAdjust}`).style.border = `5px solid blue`;
    //blink
    interval = setInterval(() => {blink()}, 650);
    if(all == "All"){nmbToShow = "All";}
}
function resetBorder(){
    var all = nmbToShow;
    if(all == "All"){nmbToShow = array1.length;}
    //reset border
    for(var i = 0; i < nmbToShow; i++){
        if(i == array1.length){break;};
        document.getElementById(`myCarousel${i+1}`).style.border = `5px solid black`;
    }
    if(all == "All"){nmbToShow = "All";}
}
function blink(){
    var all = nmbToShow;
    if(all == "All"){nmbToShow = array1.length;}
    //blink
    for(var i=0; i < nmbToShow; i++){
        if(i == array1.length){break;};
        var data = document.getElementById(`myCarousel${i+1}`);
        if(data.style.border != `5px solid black`){
            data.style.border = (data.style.border == `5px solid blue`? "5px solid transparent":`5px solid blue`);
        }
    }
    if(all == "All"){nmbToShow = "All";}
}