public class Solution_1 {
  public static void main(String[] args) {
    String str = "asdfoifu";

    if (str.matches("[a-zA-Z]+")){
      System.out.print(isUnique(str));
    }
    else {
      System.out.print("Error: Invalid Input => Alphabets only");
    }
  }

  public static boolean isUnique(String input) {
    for (int i = 0; i < input.length(); i++) {
      char alphabet = input.charAt(i);
      String target = input.substring(i + 1);
      int index_checker = target.indexOf(alphabet);

      if (index_checker != -1) return false;
    }

    return true;
  }
}